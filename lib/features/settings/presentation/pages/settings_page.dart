import 'package:flutter/material.dart';

import '../../../../routes.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: ListView(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.color_lens),
            title: Text('Change Theme'),
            onTap: () {
              Routes.sailor.navigate('/select_theme_screen');
            },
          )
        ],
      ),
    );
  }
}
