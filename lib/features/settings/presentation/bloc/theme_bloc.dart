import 'dart:async';
import 'package:flutter/material.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:resocoder_architecture/features/settings/domain/entities/app_themes.dart';
import './bloc.dart';

class ThemeBloc extends HydratedBloc<ThemeEvent, ThemeState> {
  @override
  ThemeState get initialState {
    return super.initialState ??
        ThemeState(themeData: appThemeData[AppTheme.GreenLight]);
  }

  @override
  Stream<ThemeState> mapEventToState(
    ThemeEvent event,
  ) async* {
    if (event is ThemeChanged) {
      yield ThemeState(themeData: appThemeData[event.theme]);
    }
  }

  @override
  ThemeState fromJson(Map<String, dynamic> json) {
    final themeData = ThemeData(
        primaryColor: _mapNameToColor(json['primaryColor']),
        brightness: _mapNameToBrightness(json['brightness']));
    return ThemeState(
      themeData: themeData,
    );
  }

  @override
  Map<String, dynamic> toJson(ThemeState state) {
    final primaryColor = state.themeData.primaryColor;
    final brightness = state.themeData.brightness;

    final stateJson = {
      'primaryColor': _mapColorToName(primaryColor),
      'brightness': _mapBrightnessToName(brightness),
    };
    return stateJson;
  }

  Color _mapNameToColor(String name) {
    switch (name) {
      case 'green':
        return Colors.green;
      case 'darkGreen':
        return Colors.green[700];
      case 'blue':
        return Colors.blue;
      case 'darkBlue':
        return Colors.blue[700];
      default:
        return Colors.lightBlue;
    }
  }

  Brightness _mapNameToBrightness(String name) {
    switch (name) {
      case 'light':
        return Brightness.light;
      case 'dark':
        return Brightness.dark;
      default:
        return Brightness.light;
    }
  }

  String _mapColorToName(Color color) {
    if (color == Colors.green) {
      return 'green';
    } else if (color == Colors.green[700]) {
      return 'darkGreen';
    } else if (color == Colors.blue) {
      return 'blue';
    } else if (color == Colors.blue[700]) {
      return 'darkBlue';
    } else {
      return 'lightBlue';
    }
  }

  String _mapBrightnessToName(Brightness brightness) {
    if (brightness == Brightness.light) {
      return 'light';
    } else if (brightness == Brightness.dark) {
      return 'dark';
    }
    return 'light';
  }
}
