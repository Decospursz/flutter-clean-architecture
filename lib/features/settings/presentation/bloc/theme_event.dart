import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:resocoder_architecture/features/settings/domain/entities/app_themes.dart';

@immutable
abstract class ThemeEvent extends Equatable {
  const ThemeEvent();
}

class ThemeChanged extends ThemeEvent {
  final AppTheme theme;

  ThemeChanged({@required this.theme});

  @override
  List<Object> get props => [theme];
}
