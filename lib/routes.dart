import 'package:resocoder_architecture/features/number_trivia/presentation/pages/number_trivia_page.dart';
import 'package:resocoder_architecture/features/settings/presentation/pages/select_theme_page.dart';
import 'package:resocoder_architecture/features/settings/presentation/pages/settings_page.dart';
import 'package:sailor/sailor.dart';

class Routes {
  static final sailor = Sailor();

  static void createRoutes() {
    sailor.addRoutes([
      // Just for good measure, we won't explicitly navigate to the InitialPage.
      SailorRoute(
        name: '/home_screen',
        builder: (context, args, params) {
          return NumberTriviaPage();
        },
      ),
      SailorRoute(
        name: '/settings_screen',
        builder: (context, args, params) {
          return SettingsPage();
        },
      ),
      SailorRoute(
        name: '/select_theme_screen',
        builder: (context, args, params) {
          return SelectThemePage();
        },
      ),
    ]);
  }
}
